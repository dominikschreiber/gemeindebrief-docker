FROM ubuntu
MAINTAINER domischreib

# be up-to-date
RUN apt-get update
RUN apt-get -y upgrade

# install jekyll
RUN apt-get install -y build-essential ruby-dev rubygems libffi-dev
RUN gem install jekyll bundler
RUN jekyll new /gemeindebrief
RUN bundle config --global silence_root_warning 1
RUN cd /gemeindebrief && bundle install
VOLUME ["/gemeindebrief/_posts"]

# install weasyprint
RUN apt-get install -y libcairo2-dev libpango1.0-dev libgdk-pixbuf2.0-dev python-pip python-dev
RUN pip install --upgrade pip virtualenv
RUN mkdir /weasyprint
RUN cd /weasyprint
RUN virtualenv --system-site-packages ./venv
RUN . ./venv/bin/activate
RUN pip install WeasyPrint

# weasyprint the jekyll site when called
CMD cd /gemeindebrief && bundle exec jekyll serve & weasyprint http://localhost:4000 /gemeindebrief/_posts/gemeindebrief.pdf
